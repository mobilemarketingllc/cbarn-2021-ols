<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );
	wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
	wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
	  wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
	  wp_enqueue_style( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
	  wp_enqueue_script( 'bootstrap-js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), true);
}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );



remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
	if ( isset( $atts['facet'] ) ) {       
		$output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
	}
	return $output;
  }, 10, 2 );

  
add_filter( 'facetwp_query_args', function( $query_args, $class ) {
	if( @$query_args['is_featured_temp'] == '1'){
	  $query_args['posts_per_page']='4';
	  $query_args['orderby'] = 'date';

	}else{
	  $query_args['posts_per_page']='12';
	  $query_args['meta_key']='collection';
	  if(($query_args['post_type'] !="fl-builder-template" && $query_args['post_type'] !="post" && $query_args['post_type'] !="product" && $query_args['post_type'] !="astra-advanced-hook"  && $query_args['post_type'] !="slick-slider" && $query_args['post_type'] != "acf-field") &&  @wp_count_posts($query_args['post_type'])->publish > 1 ){
		  add_filter('posts_groupby', 'query_group_by_filter');
	  }
	  
	}  
	  
  return $query_args;
}, 10, 2 );


//Roomvo script
add_action('astra_body_bottom', 'astra_roomvo_custom_footer_js');
function astra_roomvo_custom_footer_js() {

    $website_json_data = json_decode(get_option('website_json'));

    foreach($website_json_data->sites as $site_cloud){
            
        if($site_cloud->instance == 'prod'){
    
            if( $site_cloud->roomvo == 'true'){
    
  echo "<script src='https://www.roomvo.com/static/scripts/b2b/mobilemarketing.js' async></script>";

            }
        }
    }
}